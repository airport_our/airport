#include "mainform.h"
#include "dialog.h"
#include "ui_mainform.h"
#include <QString>
#include <QFile>
#include <QTextCodec>
#include <QTextStream>
#include "QTime"

MainForm::MainForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MainForm)
{
    ui->setupUi(this);

    QTextCodec *codec = QTextCodec::codecForName("UTF8");
    QTextCodec::setCodecForLocale(codec);

    // (! )здесь нужно передать параметром введённое имя из первой формы, я пока хз как (
    QString name = "acc"; // <------------------вот сюда вместо константы переданное с первой формы имя
    ui->player_name->setText(name);

    // в основном файле записан кошелёк, выводится в перменную
    double my_money;
    QFile money(name + ".txt");
    money.open(QIODevice::ReadWrite);
    QTextStream stream_money(&money);
    my_money = stream_money.readLine().toDouble();
    ui->purse->setText(QString::number(my_money));
    money.close();

    //считается из файла количество имеющихся самолётов
    QFile fplane0(name + "_aircrafts.txt");
    fplane0.open(QIODevice::ReadOnly);
    QTextStream streamplane0(&fplane0);
    int k = 0;
    while(!streamplane0.atEnd()) {
        QString str = streamplane0.readLine();
        k++;
    }
    k /= 4;
    fplane0.close();

    // вывод самолётов
    QFile fplane(name + "_aircrafts.txt"); // файл с самолётами пользователя
    fplane.open(QIODevice::ReadOnly);
    QTextStream streamplane(&fplane); //поток на файл
    streamplane.setCodec("windows1251"); //у потока меняется кодировка, без этой строчки не работает

    // считывается из файла в структуру и выводится
    struct_plane *plane = new struct_plane[k];
    for (int i = 0; i < k; i++) {
        plane[i].title = streamplane.readLine().toUtf8();
        plane[i].type = streamplane.readLine().toUtf8();
        plane[i].volume = streamplane.readLine().toUtf8();
        plane[i].range = streamplane.readLine().toUtf8();
        new_plane = new QStandardItem(QString(plane[i].title));
        plane_model->setItem(i, 0, new_plane);
        if (plane[i].type == "грузовой")
            new_plane = new QStandardItem(QString("Cамолёт '" + plane[i].title + "', " + plane[i].type + ", грузоподъёмность самолёта: " + plane[i].volume + " тонн, допустимая дальность: " + plane[i].range + "км."));
        else
            new_plane = new QStandardItem(QString("Cамолёт '" + plane[i].title + "', " + plane[i].type + ", вместимость самолёта: " + plane[i].volume + " человека, допустимая дальность: " + plane[i].range + "км."));
        plane_model->setItem(i, 1, new_plane);
    }
    // модель отдаётся таблицие
    ui->tableView_aircraft->setModel(plane_model);
    ui->tableView_aircraft->setColumnHidden(1, true);
    fplane.close();


    // связать данные о рейсе с данными имеющихся самолётов (!)
    // генерация рейсов из файла с городами
    QTime midnight(0,0,0);
    qsrand(midnight.secsTo(QTime::currentTime()));

    QFile fcity("CITY.txt");
    fcity.open(QIODevice::ReadOnly);
    QTextStream stream_city(&fcity);
    stream_city.setCodec("windows1251");
    int n, need = 7; // need - сколько нужно сгенерировать рейсов
    n = stream_city.readLine().toInt(); // количество городов в файле

    QString *city = new QString[n];
    // города считываются в массив
    for (int i = 0; i < n; i++) {
        city[i] = stream_city.readLine().toUtf8();
    }

    // здесь рандомно заполняется структура из семи рейсов
    struct_fls *fls = new struct_fls[need];
    int a, b;
    int i = 0;
    while (i < need) {
        a = qrand() % n;
        b = qrand() % n;
        if (a != b) {
            fls[i].title = city[qrand() % n] + " - " + city[qrand() % n];
            if (qrand() % 2 == 1)
                fls[i].type1 = "регулярный";
            else
                fls[i].type1 = "разовый";
            if (qrand() % 2 == 1)
                fls[i].type2 = "грузовой";
            else
                fls[i].type2 = "пассажирский";
            fls[i].price = qrand() % 10000 + 200;
            i++;
        }
    }
    i = 0;
    fcity.close();

    // вывод рейсов
    while(i < need){
        new_fls = new QStandardItem(QString(fls[i].title));
        fls_model->setItem(i, 0, new_fls);
        new_fls = new QStandardItem(QString("Рейс '" + fls[i].title + "': " + fls[i].type1 + ", " + fls[i].type2 + ", стоимость: " + QString::number(fls[i].price) + " рублей."));
        fls_model->setItem(i, 1, new_fls);
        i++;
    }
    i = 0;
    ui->tableView_flights->setModel(fls_model);
    ui->tableView_flights->setColumnHidden(1, true);
}

MainForm::~MainForm()
{
    delete ui;
}

// при клике на самолёт в таблице вывод информации о нём
void MainForm::on_tableView_aircraft_clicked(const QModelIndex &index)
{
    int row = ui->tableView_aircraft->currentIndex().row();
    ui->textEdit_information->setText(ui->tableView_aircraft->model()->data(ui->tableView_aircraft->model()->index(row, 1)).toString());
}

// при клике на рейс в таблице вывод информации о нём
void MainForm::on_tableView_flights_clicked(const QModelIndex &index)
{
    int row = ui->tableView_flights->currentIndex().row();
    ui->textEdit_information->setText(ui->tableView_flights->model()->data(ui->tableView_flights->model()->index(row, 1)).toString());
}

// выход
void MainForm::on_pb_end_game_clicked()
{
    Dialog * first = new Dialog();
    first->show();
    this->hide();
}
