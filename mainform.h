#ifndef MAINFORM_H
#define MAINFORM_H

#include <QDialog>
#include "QStandardItemModel"
#include "QStandardItem"

namespace Ui {
class MainForm;
}

// самолётики
struct struct_plane {
    QString title;
    QString type;
    QString volume;
    QString range;
};

// рейсы
struct struct_fls {
    QString title;
    QString type1;
    QString type2;
    double price;
};

class MainForm : public QDialog
{
    Q_OBJECT

public:
    // модель - самолёты
    QStandardItemModel *plane_model = new QStandardItemModel;
    QStandardItem *new_plane;
    // модель - рейсы
    QStandardItemModel *fls_model = new QStandardItemModel;
    QStandardItem *new_fls;

    explicit MainForm(QWidget *parent = 0);
    ~MainForm();

private slots:
    void on_tableView_aircraft_clicked(const QModelIndex &index);

    void on_tableView_flights_clicked(const QModelIndex &index);

    void on_pb_end_game_clicked();

private:
    Ui::MainForm *ui;
};

#endif // MAINFORM_H
