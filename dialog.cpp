#include "dialog.h"
#include "mainform.h"
#include "ui_dialog.h"
#include <QFile>
#include <QString>
//comment проверка отправляемости в битбакет
Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    MainForm *main_form = new MainForm();
    main_form -> show();
    this->hide();
}

void Dialog::on_lineEdit_cursorPositionChanged(int arg1, int arg2)
{
    if(ui->lineEdit->text().isEmpty())
        {ui->pushButton->setEnabled(false);}
        else {ui->pushButton->setEnabled(true);}
}
